
declare module '@ckeditor/ckeditor5-build-classic' {
    const ClassicEditorBuild: any;

    export = ClassicEditorBuild;
}
declare module 'quill-image-resize-module-react' {
    const ImageResize: any;
    export default ImageResize;
}

