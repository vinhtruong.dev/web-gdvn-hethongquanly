export interface ReportAssignments {
  lsid?: number | null;
  moTa: string;
  tienDo: number;
  thoiGian: string|null;
}
