/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  // output: "export",
  transpilePackages: ['@mui/x-charts']
}

module.exports = nextConfig
