export interface Supplier {
    nhaCungCapID: number,
    tenCongTy: string,
    maSoThue: string,
    diaChi: string,
    nguoiDaiDien: string,
    ghiChu: string,
    tinhID: number,
    nhanVienPhuTrach: string,
    chucVu: string,
    soDienThoai: string,
    loaiNCCID: number,
    nhanVienID:number
}
export interface SupplierType {
    loaiNCCID: number,
    tenLoai: string
}