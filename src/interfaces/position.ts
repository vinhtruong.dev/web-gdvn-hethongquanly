export interface Position {
    chucVuID: number,
    tenChucVu: string,
    chiTiet: string,
    phongBanID: number
}